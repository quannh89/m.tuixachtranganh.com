<?php
/**
 * Application level View Helper
 *
 * This file is application-wide helper file. You can put all
 * application-wide helper-related methods here.
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.View.Helper
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

App::uses('HtmlHelper', 'View');

/**
 * Application helper
 *
 * Add your application-wide methods in the class below, your helpers
 * will inherit them.
 *
 * @package       app.View.Helper
 */
class CommonHelper extends HtmlHelper
{
    //todo - limit text by word
    function limit_word(){

    }
    //todo - create menu layout
    function create_menu()
    {
    }

    function display_footer()
    {
        $footer_html = '<div class="page_wrapper_3 clearfix">
    <footer id="footer" class="container">
        <div class="row modules">
            <section class="col-sm-3 block block_category_footer">
                <h4>Thương hiệu<i class="icon-plus-sign"></i></h4>
                <ul class="toggle_content list-footer tree dhtml">
                    <li class="category_3">
                        <a href="http://tuixachtranganh.com/thuong-hieu/louis-vuitton/m11" title="NHÃN HIỆU THỜI TRANG XA XỈ LOUIS VUITTON">Louis Vuiton</a>
                        <ul>
                            <li class="category_8">
                                <a href="http://tuixachtranganh.com/thuong-hieu/louis-vuitton/m11"  title="NHÃN HIỆU THỜI TRANG XA XỈ LOUIS VUITTON">Louis Vuiton</a>
                            </li>
                        </ul>
                    </li>
                    <li class="category_4">
                        <a href="http://tuixachtranganh.com/thuong-hieu/hermes/m10"  title="NHÃN HIỆU THỜI TRANG XA XỈ HERMES ">Hermes</a>
                    </li>

                    <li class="category_5">
                        <a href="http://tuixachtranganh.com/thuong-hieu/chanel/m7" title="NHÃN HIỆU THỜI TRANG CHANEL">Chanel</a>
                    </li>
                </ul>
            </section>

            <section class="block blockcms_footer col-sm-3">
                <h4 class="toggle">Thông tin<i class="icon-plus-sign"></i></h4>
                <ul class="list-footer toggle_content clearfix">
                    <li class="first_item">
                        <a href="http://tuixachtranganh.com/lien-he.html#lien-he" title="Liên hệ">Liên hệ</a>
                    </li>
                    <li class="item"><a href="http://tuixachtranganh.com/lien-he.html#ban-do" title="Bản đồ">Bản đồ</a></li>
                    <li class="item"><a href="http://tuixachtranganh.com/thanh-toan.html" title="Thanh toán">Thanh toán</a></li>
                </ul>

            </section>
            <section class="bottom_footer">&copy; 2014 Bản quyền thuộc về <a target="_blank" href="http://www.tuixachtranganh.com">Túi Xách Trang Anh</a>&trade;</section>
            <section class="block blockmyaccountfooter col-sm-3">
                <h4>Liên kết ngoài<i class="icon-plus-sign"></i></h4>
                <ul class="list-footer toggle_content clearfix">
                    <li><a href="https://www.facebook.com/tuixachtranganh" target="_blank" title="Facebook" rel="nofollow">Facebook</a></li>
                    <li><a href="http://loiviet.vn/" target="_blank" title="Lời Việt" rel="nofollow">Lời Việt</a></li>
                    <li><a href="http://enbac.com/tuixachtranganh" target="_blank" title="Én Bạc" rel="nofollow">Én Bạc</a></li>
                    <li><a href="http://muare.vn/threads/49/2610103/" target="_blank" title="Diễn Đàn Mua Rẻ" rel="nofollow">Diễn Đàn Mua Rẻ</a></li>
                    <li><a href="http://www.lamchame.com/forum/showthread.php/1132398-T%C3%BAi-X%C3%A1ch-Trang-Anh-Com-H%C3%A0ng-m%E1%BB%9Bi-v%E1%BB%81-ng%C3%A0y-19-10-chuy%C3%AAn-Hermes-LV-Prada-D-amp-G-Dior-v-v" target="_blank" title="Diễn Đàn Làm Cha Mẹ" rel="nofollow">Diễn Đàn Làm Cha Mẹ</a></li>
                </ul>
            </section>
            <section class="block blockcontactinfos col-sm-3">
                <h4>Liên hệ<i class="icon-plus-sign"></i></h4>
                <ul class="toggle_content">
                    <li>141 Phố Yên Phụ, Tây Hồ, Hà Nội</li>
                    <li class="tel">0977 128 108 - 0944 17 6666</li>
                    <li><a href="mailto:tuixachtranganh@yahoo.com" >tuixachtranganh@yahoo.com</a></li>
                </ul>
            </section>
        </div>
    </footer>
</div>
</div>';

        $footer_html .= '
    <script type="text/javascript">
        var _gaq = _gaq || [];
        _gaq.push(["_setAccount", "UA-35052244-1"]);
        _gaq.push(["_trackPageview"]);

        (function() {
            var ga = document.createElement("script"); ga.type = "text/javascript"; ga.async = true;
            ga.src = ("https:" == document.location.protocol ? "https://ssl" : "http://www") + ".google-analytics.com/ga.js";
            var s = document.getElementsByTagName("script")[0]; s.parentNode.insertBefore(ga, s);
        })();

jQuery(document).ready(function() {
    jQuery(".shtlikebox").hover(
        function() {jQuery(this).stop().animate({right: "0"}, "medium");},
        function() {jQuery(this).stop().animate({right: "-207"}, "medium");}, 500
    );
});
</script>';
    return $footer_html;
    }

}

