<?php
$cakeDescription = __d('cake_dev', 'Túi Xách Trang Anh');
?>
<!DOCTYPE HTML>
<!--[if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7 " lang="en"> <![endif]-->
<!--[if IE 7]><html class="no-js lt-ie9 lt-ie8 ie7" lang="en"> <![endif]-->
<!--[if IE 8]><html class="no-js lt-ie9 ie8" lang="en"> <![endif]-->
<!--[if gt IE 8]> <html class="no-js ie9" lang="en"> <![endif]-->
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
<head>
    <?php echo $this->Html->charset(); ?>
    <title>
        <?php echo $cakeDescription ?>:
        <?php echo $title_for_layout; ?>
    </title>
    <meta name="description" content="<?php echo $cakeDescription;?>" />
    <?php 		echo $this->Html->meta('icon','url is here');?>
    <?php 		echo $this->Html->meta('image/x-icon','url is here');?>
    <meta name="generator" content="PrestaShop" />
    <meta name="robots" content="index,follow" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <link href='http://fonts.googleapis.com/css?family=Roboto&subset=latin,cyrillic-ext,cyrillic' rel='stylesheet' type='text/css'/>
    <link href='http://fonts.googleapis.com/css?family=Roboto:300&subset=latin,cyrillic-ext,cyrillic' rel='stylesheet' type='text/css'/>
    <link href='http://fonts.googleapis.com/css?family=Roboto:700&subset=latin,cyrillic-ext,cyrillic' rel='stylesheet' type='text/css'/>
    <script>
        if (navigator.userAgent.match(/Android/i)) {
            var viewport = document.querySelector("meta[name=viewport]");
            viewport.setAttribute('content', 'initial-scale=1.0,maximum-scale=1.0,user-scalable=0,width=device-width,height=device-height');
        }
        if(navigator.userAgent.match(/Android/i)){
            window.scrollTo(0,1);
        }
    </script>
    <script type="text/javascript">
        var baseDir = '';
        var baseUri = '';
        var static_token = '1c2a242deb6ffaa26af953278ebb6fd1';
        var token = '2eb7a43c33fd4a9c2fe133f31f951916';
        var priceDisplayPrecision = 2;
        var priceDisplayMethod = 1;
        var roundMode = 2;
    </script>
    <?php
        //todo - import css
        //echo $this->Html->css('cake.generic');
        echo $this->Html->css('bootstrap.min');
        echo $this->Html->css('font.css');
        echo $this->Html->css('grid_prestashop');
        echo $this->Html->css('global');
        echo $this->Html->css('style');
        echo $this->Html->css('blockbestsellers');
        echo $this->Html->css('blockpermanentlinks');
        echo $this->Html->css('blockviewed');
        echo $this->Html->css('blockcart');
        echo $this->Html->css('blocksocial');
        echo $this->Html->css('blockcontact');
        echo $this->Html->css('blockcategories');
        echo $this->Html->css('blockspecials');
        echo $this->Html->css('blockcurrencies');
        echo $this->Html->css('blocknewproducts');
        echo $this->Html->css('blockmyaccount');
        echo $this->Html->css('blockuserinfo');
        echo $this->Html->css('blocklanguages');
        echo $this->Html->css('blockmanufacturer');
        echo $this->Html->css('blockcms');
        echo $this->Html->css('blocktags');
        echo $this->Html->css('blockstore');
        echo $this->Html->css('jquery.autocomplete');
        echo $this->Html->css('product_list');
        echo $this->Html->css('blocksearch');
        echo $this->Html->css('blockcontactinfos');
        echo $this->Html->css('favoriteproducts');
        echo $this->Html->css('homefeatured');
        echo $this->Html->css('blocknewsletter');
        echo $this->Html->css('blocksupplier');
        echo $this->Html->css('blockwishlist');
        echo $this->Html->css('blocklink');
        echo $this->Html->css('nivo-slider');
        echo $this->Html->css('hooks');
        echo $this->Html->css('crossselling');
        echo $this->Html->css('productscategory');
        echo $this->Html->css('sendtoafriend');
        echo $this->Html->css('productcomments');
        echo $this->Html->css('superfish-modified');
        echo $this->Html->css('responsive');

        //todo - import javascript
        echo $this->Html->script('jquery-1.7.2.min');//1 file
        echo $this->Html->script('jquery-migrate-1.2.1');//1 file
        echo $this->Html->script('jquery.easing');//1 file
        echo $this->Html->script('tools');//1 file
        echo $this->Html->script('carriercompare');//1 file
        echo $this->Html->script('treeManagement');//1 file
        echo $this->Html->script('jquery.autocomplete');//1 file
        echo $this->Html->script('favoriteproducts');//1 file
        echo $this->Html->script('ajax-wishlist');//1 file
        echo $this->Html->script('jquery.nivo.slider.pack');//1 file
        echo $this->Html->script('crossselling');//1 file
        echo $this->Html->script('productscategory');//1 file
        echo $this->Html->script('jquery.serialScroll');//1 file
        echo $this->Html->script('hoverIntent');//1 file
        echo $this->Html->script('superfish-modified');//1 file
        echo $this->Html->script('bootstrap.min');//1 file
        echo $this->Html->script('mainscript');//1 file
        echo $this->Html->script('jquery.core-ui-select');//1 file
        echo $this->Html->script('jquery.scrollpane');//1 file
        echo $this->Html->script('jquery.uniform');//1 file
        echo $this->Html->script('plugins');//1 file
        echo $this->Html->script('footable');//1 file
        echo $this->Html->script('jquery.mousewheel');//1 file
        echo $this->Html->script('jquery.carouFredSel-6.2.1');//1 file
        echo $this->Html->script('jquery.touchSwipe.min');//1 file
    ?>
</head>

<html>
<body id="index" class="index lang_en">

<div id="page" class="clearfix">
<div class="page_wrapper_1 clearfix">
    <!-- Header -->
    <header id="header" class="container">
        <div id="header_right">
            <a id="header_logo" href="" title="New store">
                <img class="logo" src="<?php echo $this->webroot;?>img/logo.jpg" alt="New store"  width="260"  height="56"/>
            </a>

            <!-- Block permanent links module HEADER -->
            <section class="header-box blockpermanentlinks-header">
                <ul id="header_links" class="hidden-xs">
                    <li><a href="http://livedemo00.template-help.com/prestashop_47933/index.php" class="header_links_home">home</a></li>
                    <li id="header_link_contact"><a class="header_links_contact" href="http://livedemo00.template-help.com/prestashop_47933/index.php?controller=contact" title="Contact">Contact</a></li>
                    <li id="header_link_sitemap"><a class="header_links_sitemap" href="http://livedemo00.template-help.com/prestashop_47933/index.php?controller=sitemap" title="Sitemap">Sitemap</a></li>
                    <li id="header_link_bookmark">
                        <script type="text/javascript">writeBookmarkLink('http://livedemo00.template-help.com/prestashop_47933/', 'New store', 'bookmark');</script>
                    </li>


                </ul>

                <div class="mobile-link-top header-button visible-xs">
                    <h4 class="icon_wrapp">
                        <span class="title-hed"></span><i class="arrow_header_top_menu arrow_header_top icon-reorder"></i>
                    </h4>
                    <ul id="mobilelink" class="list_header">
                        <li><a href="#" class="header_links_home">home</a></li>
                        <li id="header_link_contact"><a class="header_links_contact" href="#" title="Contact">Contact</a></li>
                        <li id="header_link_sitemap"><a class="header_links_sitemap" href="#" title="Sitemap">Sitemap</a></li>
                        <li id="header_link_bookmark">
                            <script type="text/javascript">writeBookmarkLink('#', 'New store', 'bookmark');</script>
                        </li>
                    </ul>
                </div>
            </section>
            <!-- /Block permanent links module HEADER -->


            <!-- block seach mobile -->
            <!-- Block search module TOP -->
            <section id="search_block_top" class="header-box">
                <form method="get" action="#" id="searchbox">
                    <p>
                        <label for="search_query_top">Search</label>
                        <input type="hidden" name="controller" value="search" />
                        <input type="hidden" name="orderby" value="position" />
                        <input type="hidden" name="orderway" value="desc" />
                        <input class="search_query" type="text" id="search_query_top" name="search_query" value="" />
                        <a href="javascript:document.getElementById('searchbox').submit();"><span>Search</span></a>

                    </p>
                </form>
            </section>
           <?php
           //$categories = $this->ProductCategory->find('all');
           //var_dump($categories);die();
           //$this->Common->create_menu();
           ?>
            <script type="text/javascript">
                $(document).ready(function() {
                    $('#menu-custom li:has(ul)').addClass('hasSub');
                });
            </script>

            <div id="minic_slider" class="theme-default">
                <div id="slider" class="nivoSlider" style="min-height:427px;">
                    <a href="index.php?id_category=3&controller=category" >                        <img src="<?php echo $this->webroot;?>img/slide1.jpg" class="slider_image"
                                                                                                        title="#htmlcaption_1"
                            />
                    </a>                                    <a href="index.php?id_category=4&controller=category" >                        <img src="<?php echo $this->webroot;?>img/slide2.jpg" class="slider_image"
                                                                                                                                                title="#htmlcaption_2"
                            />
                    </a>                                    <a href="index.php?id_category=5&controller=category" >                        <img src="<?php echo $this->webroot;?>img/slide3.jpg" class="slider_image"
                                                                                                                                                title="#htmlcaption_3"
                            />
                    </a>                            </div>
                <div id="htmlcaption_1" class="nivo-html-caption">
                    <h3></h3>
                    <p><h2>Cross Body</h2>
                    <h3>get up to</h3>
                    <h4>50% off</h4>
                    <h5>Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</h5>
                    <a class="btn1" href="index.php?id_category=3&controller=category">Shop now!</a></p>
                </div>
                <div id="htmlcaption_2" class="nivo-html-caption">
                    <h3></h3>
                    <p><h2>Clutches</h2>
                    <h3>get up to</h3>
                    <h4>50% off</h4>
                    <h5>Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</h5>
                    <a class="btn1" href="index.php?id_category=4&controller=category">Shop now!</a></p>
                </div>
                <div id="htmlcaption_3" class="nivo-html-caption">
                    <h3></h3>
                    <p><h2>Backpacks</h2>
                    <h3>get up to</h3>
                    <h4>50% off</h4>
                    <h5>Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</h5>
                    <a class="btn1" href="index.php?id_category=5&controller=category">Shop now!</a></p>
                </div>
            </div>
            <script type="text/javascript">
                $(window).load(function() {
                    $('#slider').nivoSlider({
                        effect: 'fade',
                        slices: 15,
                        boxCols: 8,
                        boxRows: 4,
                        animSpeed: 500,
                        pauseTime: 3000,
                        startSlide: 0,
                        directionNav: false,
                        controlNav: true,
                        controlNavThumbs: false,
                        pauseOnHover: true,
                        manualAdvance: false,
                        prevText: 'Prev',
                        nextText: 'Next',
                        randomStart: false,

                        afterLoad: function(){
                            $('#slider').css({'min-height' : '1px'});
                        }

                    });
                });
            </script>
            <div id="htmlcontent_top">
                <ul class="htmlcontent-home row">
                    <li class="htmlcontent-item num-1">
                        <div>
                            <a href="index.php?id_category=3" class="item-link">
                                <img src="<?php echo $this->webroot;?>img/9d85b2c7e0150f6d3e4cb23a78920b87.jpg" class="item-img" alt="" />
                            </a>
                        </div>
                    </li>
                    <li class="htmlcontent-item num-2">
                        <div>
                            <a href="index.php?id_category=4" class="item-link">
                                <img src="<?php echo $this->webroot;?>img/6f310144df91156cb836afc28e7bcd95.jpg" class="item-img" alt="" />
                            </a>
                        </div>
                    </li>
                    <li class="htmlcontent-item num-3">
                        <div>
                            <a href="index.php?id_category=5" class="item-link">
                                <img src="<?php echo $this->webroot;?>img/651acac8dc7402296a2f653356bbc0e9.jpg" class="item-img" alt="" />
                            </a>
                        </div>
                    </li>
                </ul>
            </div>

        </div>
    </header>
</div>
<div class="page_wrapper_2 clearfix">
    <?php echo $this->fetch('content'); ?>
</div>
<!-- Footer -->
<?php echo $this->Common->display_footer();?>
</div>
</body>
</html>
