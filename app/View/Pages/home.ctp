<div id="columns" class="container">
    <div class="row">
        <!-- Left -->
        <!-- Center -->
        <div id="center_column" class="center_column col-xs-12 col-sm-12 clearfix">
            <!-- MODULE Home Featured Products -->
            <div id="featured-products_block_center" class="main_page_products block">
                <h4><span>Kênh Video</span></h4>
                <ul class="row">
                    <?php foreach($data as $key => $value):
                        if(trim($value["Video"]["title"]) == "" ){
                            continue;
                        }
                        ?>
                        <?php if($key == 0):?>
                        <li class="ajax_block_product col-xs-12 col-sm-6 col-md-3 first_item_of_line first_item_of_tablet_line">
                    <?php elseif($key % 3 == 0):?>
                        <li class="ajax_block_product col-xs-12 col-sm-6 col-md-3 last_item_of_line last_item_of_tablet_line">
                    <?php else:?>
                        <li class="ajax_block_product col-xs-12 col-sm-6 col-md-3 last_item_of_tablet_line">
                    <?php endif;?>
                        <div class="inner_content clearfix">
                            <div class="product_image">
                                <a href="<?php echo $value["Video"]["url"];?>" title="<?php echo $value["Video"]["title"];?>" class="product_image">
                                    <img src="<?php echo $value["Video"]["img"];?>" alt="<?php echo $value["Video"]["title"];?>" width="270px" height="203px" /></a></div>
                            <h5>
                                <a class="product_link" href="<?php echo $value["Video"]["url"];?>" title="<?php echo $value["Video"]["title"];?>">
                                    <?php echo String::truncate($value["Video"]["title"], 36);
                                    ?>
                                </a>
                            </h5>
                            <p class="product_desc"> <?php echo String::truncate($value["Video"]["intro"],60);?></p>
                            <div class="product_content">
                                <a class="exclusive btn btn-default ajax_add_to_cart_button btn_add_cart" rel="ajax_id_product_1" href="<?php echo $value["Video"]["url"];?>" title="<?php echo $value["Video"]["url"];?>"><span>Xem Video</span></a>
                            </div>
                        </div>
                        </li>
                    <?php endforeach;?>
                </ul>
            </div>
            <!-- /MODULE Home Featured Products -->
        </div>
        <!-- Right -->
    </div>
</div>
<?php
?>