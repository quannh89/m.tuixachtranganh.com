<div id="columns" class="container">
            <div class="row">
                <!-- Left -->
                <!-- Center -->
                <div id="center_column" class="center_column col-xs-12 col-sm-9 clearfix">
                    <!-- MODULE Home Featured Products -->
                    <div id="featured-products_block_center" class="main_page_products block">
                        <h4><span>Kênh Video</span></h4>
                        <ul class="row">
                            <?php foreach($data as $key => $value):
                                if(trim($value["Video"]["title"]) == "" ){
                                    continue;
                                }
                            ?>
                            <?php if($key == 0):?>
                                <li class="ajax_block_product col-xs-12 col-sm-6 col-md-3 first_item_of_line first_item_of_tablet_line">
                            <?php elseif($key % 3 == 0):?>
                                <li class="ajax_block_product col-xs-12 col-sm-6 col-md-3 last_item_of_line last_item_of_tablet_line">
                            <?php else:?>
                                <li class="ajax_block_product col-xs-12 col-sm-6 col-md-3 last_item_of_tablet_line">
                            <?php endif;?>
                                <div class="inner_content clearfix">
                                    <div class="product_image">
                                        <a href="<?php echo $value["Video"]["url"];?>" title="<?php echo $value["Video"]["title"];?>" class="product_image">
                                            <img src="<?php echo $value["Video"]["img"];?>" alt="<?php echo $value["Video"]["title"];?>" width="270px" height="203px" /></a></div>
                                    <h5>
                                        <a class="product_link" href="<?php echo $value["Video"]["url"];?>" title="<?php echo $value["Video"]["title"];?>">
                                            <?php echo String::truncate($value["Video"]["title"], 25);
                                            ?>
                                        </a>
                                    </h5>
                                    <p class="product_desc"> <?php echo String::truncate($value["Video"]["intro"],40);?></p>
                                    <div class="product_content">
                                        <a class="exclusive btn btn-default ajax_add_to_cart_button btn_add_cart" rel="ajax_id_product_1" href="<?php echo $value["Video"]["url"];?>" title="<?php echo $value["Video"]["url"];?>"><span>Xem Video</span></a>
                                    </div>
                                </div>
                            </li>
                            <?php endforeach;?>
                        </ul>
<!-- Paging and Compare-->
                        <div class="bottom_pagination shop_box_row  clearfix">
<!--                            <script type="text/javascript">-->
<!--                                // <![CDATA[-->
<!--                                var min_item = "Please select at least one product";-->
<!--                                var max_item = "You cannot add more than 2 product(s) to the product comparison";-->
<!--                                //]]>-->
<!--                            </script>-->
<!--                            <form class="form_compare hidden-xs" method="post" action="" onsubmit="true">-->
<!--                                <p>-->
<!--                                    <input type="submit" id="bt_compare_bottom" class="button btn btn-default" value="Compare">-->
<!--                                    <input type="hidden" name="compare_product_list" class="compare_product_list" value="">-->
<!--                                </p>-->
<!--                            </form>-->
                            <div id="pagination_bottom" class="pagination">
                                <ul class="pagination">
                                    <?php
                                    echo $this->Paginator->prev(__('Previous'), array('tag' => 'li'), null,
                                        array('tag' => 'li','class'=> 'button','disabledTag' => 'span'));
                                    echo $this->Paginator->numbers(array('separator' => '','currentTag' => 'a', 'currentClass' => 'active','tag' => 'li','first' => 1));
                                    echo $this->Paginator->next(__('Next'), array('tag' => 'li','currentClass' => 'disabled'), null, array('tag' => 'li','class' => 'disabled','disabledTag' => 'span'));
                                    ?>
                                </ul>
                            </div>
                        </div>
<!-- /Pagination and Comparison -->
                    </div>
                </div>

                <!-- Right -->
                <div id="right_column" class="col-xs-12 col-sm-3 column">
                    <section class="block blockcms column_box">
                        <h4 class=""><span>Danh mục Video</span><i class="column_icon_toggle icon-plus-sign"></i></h4>
                        <ul class="store_list toggle_content clearfix">
                            <?php foreach($data_video_category as $cat_item):?>
                                    <li>
                                        <a href="" title="<?php echo $cat_item["VideoCategory"]["title"];?>">
                                            <i class="icon-ok"></i><?php echo $cat_item["VideoCategory"]["title"];?>
                                        </a>
                                    </li>
                            <?php endforeach;?>
                        </ul>
                    </section>
                </div>
                <!-- Right-->
            </div>
        </div>
