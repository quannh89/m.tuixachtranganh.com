<?php
/**
 * Created by PhpStorm.
 * User: quannh89
 * Date: 2/14/14
 * Time: 1:22 PM
 */
App::uses('AppController', 'Controller');
class VideosController extends AppController {
    public $user = array();
    var $helpers = array('Paginator','Html');
    var $components = array('Session');
    var $paginate = array();


    public function index(){
        //$data_video = $this->Video->find("all");
        $this->paginate = array(
            'limit' => 16,
            'order' => array('video_id' => 'desc'),
        );
        $this->loadModel('VideoCategory');
        $data_category_video = $this->VideoCategory->find("all");
        $data_video = $this->paginate("Video");

        $this->set("data_video_category",$data_category_video);
        $this->set("data",$data_video);
    }


    public function view($id){
        $data_video_detail = $this->Video->find('threaded', array(
            'conditions' => array('video_id' => $id)
        ));
        $this->set("data_video_detail",$data_video_detail);
        $this->render("view");
    }

} 